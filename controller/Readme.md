## 5G-Kube Agent draft implementation

Draft for defining a Custom Resource in Kubernetes API (AMF module example)

```
apiVersion: apiextensions.k8s.io/v1
kind: CustomResourceDefinition
metadata:
  name: amf.mmw.disi.unibo.it
spec:
  # group name to use for REST API: /apis/<group>/<version>
  group: mmw.disi.unibo.it
  names:
    # plural name to be used in the URL: /apis/<group>/<version>/<plural>
    plural: amfs
    # singular name to be used as an alias on the CLI and for display
    singular: amf
    # kind is normally the CamelCased singular name of the schema
    kind: Amf
  scope: Namespaced
  versions:
    - name: v1
      served: true
      storage: true
      schema:
        # Amf objects will have just two properties: .count and .selector
        openAPIV3Schema:
          type: object
          properties:
            spec:
              type: object
              properties:
                count:
                  type: number
                  minimum: 1
                selector:
                  type: string
                  maxLength: 1024

```

Coming soon...
